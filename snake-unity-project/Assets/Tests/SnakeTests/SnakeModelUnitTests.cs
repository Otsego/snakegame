using App.Scripts.Snake.Model;
using Zenject;
using NUnit.Framework;
using UnityEngine;
using App.Scripts.Snake.Settings;
using System;

[TestFixture]
public sealed class SnakeModelUnitTests : ZenjectUnitTestFixture
{
    [SetUp]
    public void CommonInstall()
    {
        Container.Bind<SnakeModel>().AsTransient();
        Container.BindFactory<uint, Vector2Int, Vector2Int, SnakeSegment, SnakeSegmentFactory>().AsSingle();
    }

    [Test]
    public void RunValidLengthTest()
    {
        TestLength(1);
        TestLength(2);
        TestLength(3);
        TestLength(400);
    }

    private void TestLength(uint length)
    {
        var snakeModel = Container.Resolve<SnakeModel>();
        snakeModel.Create(new SnakeStartSettings(length, Vector2Int.up, Vector2Int.zero));
        
        Assert.True(snakeModel.Length == length);
        var moves = snakeModel.Move();
        Assert.NotNull(moves);
        Assert.True(moves.Count == length);
    }
    
    [Test]
    public void RunInvalidLengthTest()
    {
        var snakeModel = Container.Resolve<SnakeModel>();
        Assert.Throws<ArgumentException>(() => snakeModel.Create(new SnakeStartSettings(0, Vector2Int.up, Vector2Int.zero)));
    }
    
    [Test]
    public void RunMovementTest1()
    {
        var snakeModel = Container.Resolve<SnakeModel>();
        snakeModel.Create(new SnakeStartSettings(1, Vector2Int.up, Vector2Int.zero));
        
        // Crawl a 2x2 circle
        var moves = snakeModel.Move();
        Assert.True(moves.Count == 1);
        Assert.True(moves[0].Position == new Vector2Int(0, 1));
        Assert.True(moves[0].SegmentNumber == 0);

        moves = snakeModel.Move();
        Assert.True(moves.Count == 1);
        Assert.True(moves[0].Position == new Vector2Int(0, 2));
        Assert.True(moves[0].SegmentNumber == 0);
        
        snakeModel.ChangeDirection(Vector2Int.left);
        moves = snakeModel.Move();
        Assert.True(moves.Count == 1);
        Assert.True(moves[0].Position == new Vector2Int(-1, 2));
        Assert.True(moves[0].SegmentNumber == 0);
        
        moves = snakeModel.Move();
        Assert.True(moves.Count == 1);
        Assert.True(moves[0].Position == new Vector2Int(-2, 2));
        Assert.True(moves[0].SegmentNumber == 0);
        
        snakeModel.ChangeDirection(Vector2Int.down);
        moves = snakeModel.Move();
        Assert.True(moves.Count == 1);
        Assert.True(moves[0].Position == new Vector2Int(-2, 1));
        Assert.True(moves[0].SegmentNumber == 0);
        
        moves = snakeModel.Move();
        Assert.True(moves.Count == 1);
        Assert.True(moves[0].Position == new Vector2Int(-2, 0));
        Assert.True(moves[0].SegmentNumber == 0);
        
        snakeModel.ChangeDirection(Vector2Int.right);
        moves = snakeModel.Move();
        Assert.True(moves.Count == 1);
        Assert.True(moves[0].Position == new Vector2Int(-1, 0));
        Assert.True(moves[0].SegmentNumber == 0);
        
        moves = snakeModel.Move();
        Assert.True(moves.Count == 1);
        Assert.True(moves[0].Position == Vector2Int.zero);
        Assert.True(moves[0].SegmentNumber == 0);

        // Check the prohibition of a movement in the opposite direction
        snakeModel.ChangeDirection(Vector2Int.left);
        moves = snakeModel.Move();
        Assert.True(moves[0].Position == new Vector2Int(1, 0));
    }

    [Test]
    public void RunMovementTest2()
    {
        var snakeModel = Container.Resolve<SnakeModel>();
        snakeModel.Create(new SnakeStartSettings(2, Vector2Int.up, Vector2Int.zero));

        // Crawl a circle
        var moves = snakeModel.Move();
        Assert.True(moves.Count == 2);
        Assert.True(moves[0].Position == new Vector2Int(0, 1));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(0, 0));
        Assert.True(moves[1].SegmentNumber == 1);
        
        moves = snakeModel.Move();
        Assert.True(moves.Count == 2);
        Assert.True(moves[0].Position == new Vector2Int(0, 2));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(0, 1));
        Assert.True(moves[1].SegmentNumber == 1);
        
        snakeModel.ChangeDirection(Vector2Int.left);
        moves = snakeModel.Move();
        Assert.True(moves.Count == 2);
        Assert.True(moves[0].Position == new Vector2Int(-1, 2));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(0, 2));
        Assert.True(moves[1].SegmentNumber == 1);
        
        moves = snakeModel.Move();
        Assert.True(moves.Count == 2);
        Assert.True(moves[0].Position == new Vector2Int(-2, 2));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(-1, 2));
        Assert.True(moves[1].SegmentNumber == 1);
        
        snakeModel.ChangeDirection(Vector2Int.down);
        moves = snakeModel.Move();
        Assert.True(moves.Count == 2);
        Assert.True(moves[0].Position == new Vector2Int(-2, 1));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(-2, 2));
        Assert.True(moves[1].SegmentNumber == 1);
        
        moves = snakeModel.Move();
        Assert.True(moves.Count == 2);
        Assert.True(moves[0].Position == new Vector2Int(-2, 0));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(-2, 1));
        Assert.True(moves[1].SegmentNumber == 1);
        
        snakeModel.ChangeDirection(Vector2Int.right);
        moves = snakeModel.Move();
        Assert.True(moves.Count == 2);
        Assert.True(moves[0].Position == new Vector2Int(-1, 0));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(-2, 0));
        Assert.True(moves[1].SegmentNumber == 1);
        
        moves = snakeModel.Move();
        Assert.True(moves.Count == 2);
        Assert.True(moves[0].Position == new Vector2Int(0, 0));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(-1, 0));
        Assert.True(moves[1].SegmentNumber == 1);
        
        // Check the prohibition of a movement in the opposite direction
        snakeModel.ChangeDirection(Vector2Int.left);
        moves = snakeModel.Move();
        Assert.True(moves[0].Position == new Vector2Int(1, 0));
        Assert.True(moves[1].Position == new Vector2Int(0, 0));
    }

    [Test]
    public void RunMovementTest3()
    {
        var snakeModel = Container.Resolve<SnakeModel>();
        snakeModel.Create(new SnakeStartSettings(3, Vector2Int.up, Vector2Int.zero));

        var moves = snakeModel.Move();
        Assert.True(moves.Count == 3);
        Assert.True(moves[0].Position == new Vector2Int(0, 1));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(0, 0));
        Assert.True(moves[1].SegmentNumber == 1);
        Assert.True(moves[2].Position == new Vector2Int(0, -1));
        Assert.True(moves[2].SegmentNumber == 2);
        
        moves = snakeModel.Move();
        Assert.True(moves.Count == 3);
        Assert.True(moves[0].Position == new Vector2Int(0, 2));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(0, 1));
        Assert.True(moves[1].SegmentNumber == 1);
        Assert.True(moves[2].Position == new Vector2Int(0, 0));
        Assert.True(moves[2].SegmentNumber == 2);
        
        moves = snakeModel.Move();
        Assert.True(moves.Count == 3);
        Assert.True(moves[0].Position == new Vector2Int(0, 3));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(0, 2));
        Assert.True(moves[1].SegmentNumber == 1);
        Assert.True(moves[2].Position == new Vector2Int(0, 1));
        Assert.True(moves[2].SegmentNumber == 2);
        
        snakeModel.ChangeDirection(Vector2Int.right);
        moves = snakeModel.Move();
        Assert.True(moves.Count == 3);
        Assert.True(moves[0].Position == new Vector2Int(1, 3));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(0, 3));
        Assert.True(moves[1].SegmentNumber == 1);
        Assert.True(moves[2].Position == new Vector2Int(0, 2));
        Assert.True(moves[2].SegmentNumber == 2);
        
        moves = snakeModel.Move();
        Assert.True(moves.Count == 3);
        Assert.True(moves[0].Position == new Vector2Int(2, 3));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(1, 3));
        Assert.True(moves[1].SegmentNumber == 1);
        Assert.True(moves[2].Position == new Vector2Int(0, 3));
        Assert.True(moves[2].SegmentNumber == 2);
        
        moves = snakeModel.Move();
        Assert.True(moves.Count == 3);
        Assert.True(moves[0].Position == new Vector2Int(3, 3));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(2, 3));
        Assert.True(moves[1].SegmentNumber == 1);
        Assert.True(moves[2].Position == new Vector2Int(1, 3));
        Assert.True(moves[2].SegmentNumber == 2);
        
        moves = snakeModel.Move();
        Assert.True(moves.Count == 3);
        Assert.True(moves[0].Position == new Vector2Int(4, 3));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(3, 3));
        Assert.True(moves[1].SegmentNumber == 1);
        Assert.True(moves[2].Position == new Vector2Int(2, 3));
        Assert.True(moves[2].SegmentNumber == 2);
        
        snakeModel.ChangeDirection(Vector2Int.up);
        moves = snakeModel.Move();
        Assert.True(moves.Count == 3);
        Assert.True(moves[0].Position == new Vector2Int(4, 4));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(4, 3));
        Assert.True(moves[1].SegmentNumber == 1);
        Assert.True(moves[2].Position == new Vector2Int(3, 3));
        Assert.True(moves[2].SegmentNumber == 2);
        
        moves = snakeModel.Move();
        Assert.True(moves.Count == 3);
        Assert.True(moves[0].Position == new Vector2Int(4, 5));
        Assert.True(moves[0].SegmentNumber == 0);
        Assert.True(moves[1].Position == new Vector2Int(4, 4));
        Assert.True(moves[1].SegmentNumber == 1);
        Assert.True(moves[2].Position == new Vector2Int(4, 3));
        Assert.True(moves[2].SegmentNumber == 2);
    }
}