using System.Collections;
using App.Scripts.Direction;
using App.Scripts.Field.Model;
using App.Scripts.Snake;
using App.Scripts.Snake.Model;
using Zenject;
using NUnit.Framework;
using UniRx;
using UnityEngine;
using UnityEngine.TestTools;
using App.Scripts.Snake.Settings;

[TestFixture]
public sealed class SnakeMoverUnitTests : ZenjectUnitTestFixture
{
    private const int FieldSize = 10;

    [SetUp]
    public void CommonInstall()
    {
        Container.Bind<SnakeModel>().AsSingle();
        Container.BindFactory<uint, Vector2Int, Vector2Int, SnakeSegment, SnakeSegmentFactory>().AsSingle();
        Container.Bind<FieldModel>().AsSingle();
        Container.Bind<SnakeMoveMaker>().AsSingle();
        Container.Bind<DirectionProvider>().AsSingle();
    }

    [UnityTest]
    public IEnumerator RunForwardMovementTest()
    {
        var testFinished = false;

        var directionProvider = Container.Resolve<DirectionProvider>();
        directionProvider.SetInitialDirection(Vector2Int.up);
        var snakeModel = Container.Resolve<SnakeModel>();
        var startPositionOnField = new Vector2Int(0, 2);
        snakeModel.Create(new SnakeStartSettings(3, Vector2Int.up, startPositionOnField));
        var snakeMoveMaker = Container.Resolve<SnakeMoveMaker>();
        var fieldModel = Container.Resolve<FieldModel>();
        var moveNumber = 0;
        fieldModel.SetDimensions(10, 10);

        snakeMoveMaker
            .SegmentDispositions
            .Skip(1)
            .Take(15)
            .Subscribe(dispositions =>
                {
                    Assert.True(dispositions.Count == 3);
                    Assert.True(dispositions[0].Position == new Vector2Int(0, (3 + moveNumber) % FieldSize));
                    Assert.True(dispositions[1].Position == new Vector2Int(0, (2 + moveNumber) % FieldSize));
                    Assert.True(dispositions[2].Position == new Vector2Int(0, (1 + moveNumber) % FieldSize));

                    moveNumber++;
                },
                () => { testFinished = true; });

        snakeMoveMaker.Run(startPositionOnField, 3);

        while (!testFinished)
        {
            yield return null;
        }
    }
}