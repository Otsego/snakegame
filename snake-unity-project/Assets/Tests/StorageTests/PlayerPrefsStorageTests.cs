using System;
using Zenject;
using NUnit.Framework;
using App.Scripts.StorageService;
using UnityEngine;
using System.Collections.Generic;

[TestFixture]
public sealed class PlayerPrefsStorageTests : ZenjectUnitTestFixture
{
    private enum Days { Sun, Mon, Tue, Wed, Thu, Fri, Sat };

    [SetUp]
    public void CommonInstall()
    {
        Container.Bind<IStorageService>().To<PlayerPrefsStorage>().AsTransient();
    }

    [Test]
    public void RunPrimitiveTypesTest()
    {
        var keyPrefix = Guid.NewGuid().ToString();
        Debug.Log($"{nameof(RunPrimitiveTypesTest)}, key prefix: {keyPrefix}");

        var storageService = Container.Resolve<IStorageService>();

        var testChar = 'c';
        storageService.Set(keyPrefix + nameof(testChar), testChar);
        var testCharSavedValue = storageService.Get<char>(keyPrefix + nameof(testChar));
        Assert.AreEqual(testChar, testCharSavedValue);

        var testBool = true;
        storageService.Set(keyPrefix + nameof(testBool), testBool);
        var testBoolSavedValue = storageService.Get<bool>(keyPrefix + nameof(testBool));
        Assert.AreEqual(testBool, testBoolSavedValue);

        var testByte = byte.MaxValue;
        storageService.Set(keyPrefix + nameof(testByte), testByte);
        var testByteSavedValue = storageService.Get<int>(keyPrefix + nameof(testByte));
        Assert.AreEqual(testByte, testByteSavedValue);

        var testInt16 = Int16.MaxValue;
        storageService.Set(keyPrefix + nameof(testInt16), testInt16);
        var testInt16SavedValue = storageService.Get<Int16>(keyPrefix + nameof(testInt16));
        Assert.AreEqual(testInt16, testInt16SavedValue);

        var testInt32 = Int32.MaxValue;
        storageService.Set(keyPrefix + nameof(testInt32), testInt32);
        var testInt32SavedValue = storageService.Get<Int32>(keyPrefix + nameof(testInt32));
        Assert.AreEqual(testInt32, testInt32SavedValue);

        var testInt64 = Int64.MaxValue;
        storageService.Set(keyPrefix + nameof(testInt64), testInt64);
        var testInt64SavedValue = storageService.Get<Int64>(keyPrefix + nameof(testInt64));
        Assert.AreEqual(testInt64, testInt64SavedValue);

        var testUInt16 = UInt16.MaxValue;
        storageService.Set(keyPrefix + nameof(testUInt16), testUInt16);
        var testUInt16SavedValue = storageService.Get<UInt16>(keyPrefix + nameof(testUInt16));
        Assert.AreEqual(testUInt16, testUInt16SavedValue);

        var testUInt32 = UInt32.MaxValue;
        storageService.Set(keyPrefix + nameof(testUInt32), testUInt32);
        var testUInt32SavedValue = storageService.Get<UInt32>(keyPrefix + nameof(testUInt32));
        Assert.AreEqual(testUInt32, testUInt32SavedValue);

        var testUInt64 = UInt64.MaxValue;
        storageService.Set(keyPrefix + nameof(testUInt64), testUInt64);
        var testUInt64SavedValue = storageService.Get<UInt64>(keyPrefix + nameof(testUInt64));
        Assert.AreEqual(testUInt64, testUInt64SavedValue);

        var testSingle = Single.MaxValue;
        storageService.Set(keyPrefix + nameof(testSingle), testSingle);
        var testSingleSavedValue = storageService.Get<Single>(keyPrefix + nameof(testSingle));
        Assert.AreEqual(testSingle, testSingleSavedValue);

        var testDouble = Double.MaxValue;
        storageService.Set(keyPrefix + nameof(testDouble), testDouble);
        var testDoubleSavedValue = storageService.Get<Double>(keyPrefix + nameof(testDouble));
        Assert.AreEqual(testDouble, testDoubleSavedValue);
    }

    [Test]
    public void RunReferenceTypesTest()
    {
        var keyPrefix = Guid.NewGuid().ToString();
        Debug.Log($"{nameof(RunReferenceTypesTest)}, key prefix: {keyPrefix}");

        var storageService = Container.Resolve<IStorageService>();

        var testString = " -=test text=- ";
        storageService.Set(keyPrefix + nameof(testString), testString);
        var testStringSavedValue = storageService.Get<String>(keyPrefix + nameof(testString));
        Assert.AreEqual(testString, testStringSavedValue);

        var testArray = new int[] { int.MinValue, 0, int.MaxValue };
        storageService.Set(keyPrefix + nameof(testArray), testArray);
        var testArraySavedValue = storageService.Get<int[]>(keyPrefix + nameof(testArray));
        Assert.AreEqual(testArray, testArraySavedValue);

        var testList = new List<int>() { int.MinValue, 0, int.MaxValue };
        storageService.Set(keyPrefix + nameof(testList), testList);
        var testListSavedValue = storageService.Get<List<int>>(keyPrefix + nameof(testList));
        Assert.AreEqual(testList, testListSavedValue);

        var testObject = new TestClass(int.MinValue, "  -=-=test=-=-  ");
        storageService.Set(keyPrefix + nameof(testObject), testObject);
        var testObjectSavedValue = storageService.Get<TestClass>(keyPrefix + nameof(testObject));
        Assert.AreEqual(testObject, testObjectSavedValue);
    }

    [Test]
    public void RunValueTypesTest()
    {
        var keyPrefix = Guid.NewGuid().ToString();
        Debug.Log($"{nameof(RunValueTypesTest)}, key prefix: {keyPrefix}");

        var storageService = Container.Resolve<IStorageService>();
     
        var testEnumValue = Days.Thu;
        storageService.Set(keyPrefix + nameof(testEnumValue), testEnumValue);
        var testEnumSavedValue = storageService.Get<Days>(keyPrefix + nameof(testEnumValue));
        Assert.AreEqual(testEnumValue, testEnumSavedValue);

        var testObject = new TestStruct(int.MinValue, "  -=-=test=-=-  ");
        storageService.Set(keyPrefix + nameof(testObject), testObject);
        var testObjectSavedValue = storageService.Get<TestStruct>(keyPrefix + nameof(testObject));
        Assert.AreEqual(testObject, testObjectSavedValue);
    }

    [Test]
    public void RunNestedTypeTest()
    {
        var keyPrefix = Guid.NewGuid().ToString();
        Debug.Log($"{nameof(RunNestedTypeTest)}, key prefix: {keyPrefix}");

        var storageService = Container.Resolve<IStorageService>();

        var testObject = new TestNestedClass("  test str1  ", new TestClass(int.MinValue, "  test str2  "), new TestStruct(int.MaxValue, " test str3  "));
        storageService.Set(keyPrefix + nameof(testObject), testObject);
        var testObjectSavedValue = storageService.Get<TestNestedClass>(keyPrefix + nameof(testObject));
        Assert.AreEqual(testObject, testObjectSavedValue);
    }

    [Serializable]
    private class TestClass : IEquatable<TestClass>
    {
        public int IntValue;
        public string StringValue;

        public TestClass(int intValue, string stringValue)
        {
            IntValue = intValue;
            StringValue = stringValue;
        }

        public bool Equals(TestClass other)
        {
            return other.IntValue == IntValue && other.StringValue == StringValue;
        }
    }

    [Serializable]
    private struct TestStruct : IEquatable<TestStruct>
    {
        public int IntValue;
        public string StringValue;

        public TestStruct(int intValue, string stringValue)
        {
            IntValue = intValue;
            StringValue = stringValue;
        }

        public bool Equals(TestStruct other)
        {
            return other.IntValue == IntValue && other.StringValue == StringValue;
        }
    }

    [Serializable]
    private class TestNestedClass : IEquatable<TestNestedClass>
    {
        public string StringValue;
        public TestClass TestRefObject;
        public TestStruct TestValObject;

        public TestNestedClass(string stringValue, TestClass testRefObject, TestStruct testValObject)
        {
            StringValue = stringValue;
            TestRefObject = testRefObject;
            TestValObject = testValObject;
        }

        public bool Equals(TestNestedClass other)
        {
            return other.StringValue == StringValue && other.TestRefObject.Equals(TestRefObject) && other.TestValObject.Equals(TestValObject);
        }
    }
}