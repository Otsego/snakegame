using App.Scripts.ResourcesManagement;
using Cysharp.Threading.Tasks;
using Framework;
using System.Threading;
using UnityEngine;
using Zenject;

public sealed class AsyncPrefabFactory<T> : IFactory<string, Transform, CancellationToken, UniTask<T>> where T : MonoBehaviour
{
    private readonly DiContainer _container;
    private readonly IAssetProvider _assetProvider;

    public AsyncPrefabFactory(DiContainer container, IAssetProvider assetProvider)
    {
        _container = container;
        _assetProvider = assetProvider;
    }

    public async UniTask<T> Create(string assetKey, Transform parent, CancellationToken token)
    {
        var prefab = await _assetProvider.LoadAsset<GameObject>(assetKey, token);

        if (token.IsCancellationRequested)
        {
            prefab.AsMaybe().Do(fab => _assetProvider.ReleaseAsset(fab));

            return default;
        }

        var instance = _container.InstantiatePrefabForComponent<T>(prefab);
        instance.transform.SetParent(parent, false);

        return instance;
    }
}

public sealed class PrefabFactory<T> : PlaceholderFactory<string, Transform, CancellationToken, UniTask<T>> where T : MonoBehaviour
{ }