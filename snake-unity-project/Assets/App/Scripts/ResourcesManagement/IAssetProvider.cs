﻿using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;

namespace App.Scripts.ResourcesManagement
{
    public interface IAssetProvider
    {
        UniTask<T> LoadAsset<T>(string assetKey, CancellationToken cancellationToken) where T : UnityEngine.Object;
        UniTask<T> Instantiate<T>(string assetKey, Transform parent, CancellationToken cancellationToken) where T : MonoBehaviour;
        void ReleaseAsset<T>(T asset) where T : UnityEngine.Object;
        void ReleaseInstance(GameObject instance);
    }
}