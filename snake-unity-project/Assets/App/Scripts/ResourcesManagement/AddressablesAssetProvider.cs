﻿using Cysharp.Threading.Tasks;
using Framework;
using JetBrains.Annotations;
using System.Threading;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace App.Scripts.ResourcesManagement
{
    [UsedImplicitly]
    public sealed class AddressablesAssetProvider : IAssetProvider
    {
        public async UniTask<T> LoadAsset<T>(string assetKey, CancellationToken cancellationToken) where T : UnityEngine.Object
        {
            var handle = Addressables.LoadAssetAsync<T>(assetKey);
            var asset = await handle;

            if (cancellationToken.IsCancellationRequested)
            {
                asset.AsMaybe().Do(ass => Addressables.Release(ass));
            }

            return asset;
        }

        public async UniTask<T> Instantiate<T>(string assetKey, Transform parent, CancellationToken cancellationToken) where T : MonoBehaviour
        {
            var handle = Addressables.InstantiateAsync(assetKey, parent);
            var instance = await handle;

            if (cancellationToken.IsCancellationRequested)
            {
                instance.AsMaybe().Do(inst => Addressables.ReleaseInstance(inst));
            }

            if (instance == null)
            {
                return null;
            }

            return instance.GetComponent<T>();
        }

        public void ReleaseAsset<T>(T asset) where T : UnityEngine.Object
        {
            Addressables.Release(asset);
        }

        public void ReleaseInstance(GameObject instance)
        {
            Addressables.ReleaseInstance(instance);
        }
    }
}