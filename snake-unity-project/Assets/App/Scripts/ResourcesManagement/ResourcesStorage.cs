using UnityEngine;

namespace App.Scripts.ResourcesManagement
{
    public sealed class ResourcesStorage : MonoBehaviour
    {
        public GameObject FieldViewPrefab => _fieldViewPrefab;

        [SerializeField] private GameObject _fieldViewPrefab;
    }
}