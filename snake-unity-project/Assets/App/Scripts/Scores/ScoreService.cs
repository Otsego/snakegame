﻿using App.Scripts.Ending.Signals;
using App.Scripts.Food.Model;
using App.Scripts.StorageService;
using JetBrains.Annotations;
using System;
using UniRx;
using UnityEngine;

namespace App.Scripts.Scores
{
    [UsedImplicitly]
    public sealed class ScoreService : IDisposable
    {
        private const string LastScoreKey = "last_score";
        private const string RecordScoreKey = "record_score";

        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        private readonly IStorageService _storageService;

        public ReactiveProperty<uint> Score { get; private set; } = new ReactiveProperty<uint>(0u);
        public uint Record => _storageService.Get<uint>(RecordScoreKey);
        public bool IsRecordBroken => Score.Value > Record;

        public ScoreService(FoodModel foodModel, IStorageService storageService)
        {
            _storageService = storageService;

            foodModel
                .FoodPositions
                .ObserveRemove()
                .Subscribe(_ => AddScore())
                .AddTo(_compositeDisposable);

            MessageBroker
                .Default
                .Receive<GameEnded>()
                .Subscribe(_ => SaveCurrentScore())
                .AddTo(_compositeDisposable);
        }

        public void Start()
        {
            TryUpdateRecordScore();
        }

        private void AddScore()
        {
            Score.Value++;
        }

        private void SaveCurrentScore()
        {
            Debug.Log($"Current score saved: {Score.Value}");

            _storageService.Set(LastScoreKey, Score.Value);
        }

        private void TryUpdateRecordScore()
        {
            var lastScore = _storageService.Get<uint>(LastScoreKey);
            var recordScore = _storageService.Get<uint>(RecordScoreKey);

            if (lastScore > recordScore)
            {
                _storageService.Set(RecordScoreKey, lastScore);
            }
        }

        public void Dispose()
        {
            _compositeDisposable?.Dispose();
        }
    }
}