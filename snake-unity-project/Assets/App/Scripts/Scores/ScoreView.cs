using App.Scripts.Scores;
using TMPro;
using UniRx;
using UnityEngine;
using Zenject;

public sealed class ScoreView : MonoBehaviour
{
    [Inject] private readonly ScoreService _scoreService;

    [SerializeField] private TMP_Text _text;

    private void Start()
    {
        _scoreService
            .Score
            .Subscribe(score => _text.text = score.ToString())
            .AddTo(this);
    }
}