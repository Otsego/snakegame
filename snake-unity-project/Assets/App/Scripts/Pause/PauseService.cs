﻿using JetBrains.Annotations;

namespace App.Scripts.Pause
{
    [UsedImplicitly]
    public sealed class PauseService
    {
        private readonly IPausable[] _pausableEntities;

        public PauseService(IPausable[] pausableEntities)
        {
            _pausableEntities = pausableEntities;
        }

        public void SetPauseStatus(bool isPaused)
        {
            if (_pausableEntities == null)
            {
                return;
            }

            foreach (var pausable in _pausableEntities)
            {
                pausable.SetPauseStatus(isPaused);
            }
        }
    }
}