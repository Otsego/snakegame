﻿using System;
using App.Scripts.Food.Model;
using App.Scripts.Food.Signals;
using Framework;
using JetBrains.Annotations;
using UniRx;
using UnityEngine;
using App.Scripts.Field;

namespace App.Scripts.Food
{
    [UsedImplicitly]
    public sealed class FoodSpawner : IDisposable
    {
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        private readonly FoodModel _foodModel;
        private readonly FieldFreePlaceProvider _fieldFreePlaceProvider;

        public FoodSpawner(FoodModel foodModel, FieldFreePlaceProvider fieldFreePlaceProvider)
        {
            _foodModel = foodModel;
            _fieldFreePlaceProvider = fieldFreePlaceProvider;
        }

        public void Start()
        {
            SpawnFood();

            _foodModel
                .FoodPositions
                .ObserveRemove()
                .Subscribe(_ => FoodRemoved())
                .AddTo(_compositeDisposable);
        }

        private void FoodRemoved()
        {
            if (_foodModel.FoodPositions.Count == 0)
            {
                SpawnFood();
            }
        }

        private void SpawnFood()
        {
            GetSpawnPosition()
                .Do(pos => _foodModel.FoodPositions.Add(pos))
                .ElseDo(() => MessageBroker.Default.Publish(new FreeSpaceForFoodEnded()));
        }

        private Maybe<Vector2Int> GetSpawnPosition()
        {
            return _fieldFreePlaceProvider.GetRandomFreePosition();
        }

        public void Dispose()
        {
            _compositeDisposable?.Dispose();
        }
    }
}