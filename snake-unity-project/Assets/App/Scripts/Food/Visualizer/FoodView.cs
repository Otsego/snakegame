using UnityEngine;
using Zenject;

namespace App.Scripts.Food.Visualizer
{
    public sealed class FoodView : MonoBehaviour
    {
        public sealed class Pool : MonoMemoryPool<FoodView>
        {
        }
    }
}