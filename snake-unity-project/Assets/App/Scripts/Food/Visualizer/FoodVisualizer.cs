﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Scripts.Food.Model;
using JetBrains.Annotations;
using UniRx;
using UnityEngine;

namespace App.Scripts.Food.Visualizer
{
    [UsedImplicitly]
    public sealed class FoodVisualizer : IDisposable
    {
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private readonly FoodView.Pool _foodViewPool;
        private readonly FoodModel _foodModel;

        private readonly List<FoodView> _foodViews = new List<FoodView>();

        public FoodVisualizer(FoodModel foodModel, FoodView.Pool foodViewPool)
        {
            _foodViewPool = foodViewPool;
            _foodModel = foodModel;

            foodModel
                .FoodPositions
                .ObserveCountChanged()
                .Subscribe(_ => Visualize())
                .AddTo(_compositeDisposable);

            Visualize();
        }

        private void Visualize()
        {
            DeactivateAllViews();

            foreach (var foodPosition in _foodModel.FoodPositions)
            {
                var view = _foodViewPool.Spawn();
                view.gameObject.name = $"Food ({foodPosition.x},{foodPosition.y})";
                view.transform.localPosition = new Vector3(foodPosition.x, foodPosition.y, -1f);
                _foodViews.Add(view);
            }
        }

        private void DeactivateAllViews()
        {
            for (var i = 0; i < _foodViews.Count; i++)
            {
                var view = _foodViews.FirstOrDefault();
                _foodViews.Remove(view);
                _foodViewPool.Despawn(view);
            }
        }

        public void Dispose()
        {
            _compositeDisposable?.Dispose();
        }
    }
}