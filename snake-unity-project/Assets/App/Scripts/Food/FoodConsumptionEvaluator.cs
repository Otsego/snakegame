﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Scripts.Data;
using App.Scripts.Food.Model;
using App.Scripts.Snake;
using JetBrains.Annotations;
using UniRx;

namespace App.Scripts.Food
{
    [UsedImplicitly]
    public sealed class FoodConsumptionEvaluator : IDisposable
    {
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        private readonly FoodModel _foodModel;

        public FoodConsumptionEvaluator(SnakeMoveMaker snakeMoveMaker, FoodModel foodModel)
        {
            _foodModel = foodModel;
            
            snakeMoveMaker
                .SegmentDispositions
                .Subscribe(ConsumeFoodIfEligible)
                .AddTo(_compositeDisposable);
        }

        private void ConsumeFoodIfEligible(IList<DispositionData> snakeSegmentsDispositions)
        {
            var snakeHeadDisposition = snakeSegmentsDispositions.FirstOrDefault();
            var nextSnakeHeadPosition = snakeHeadDisposition.Position;

            if (_foodModel.FoodPositions.Contains(nextSnakeHeadPosition))
            {
                _foodModel.FoodPositions.Remove(nextSnakeHeadPosition);
            }
        }

        public void Dispose()
        {
            _compositeDisposable?.Dispose();
        }
    }
}