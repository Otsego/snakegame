using JetBrains.Annotations;
using UniRx;
using UnityEngine;

namespace App.Scripts.Food.Model
{
    [UsedImplicitly]
    public sealed class FoodModel
    {
        public readonly ReactiveCollection<Vector2Int> FoodPositions = new ReactiveCollection<Vector2Int>();
    }
}