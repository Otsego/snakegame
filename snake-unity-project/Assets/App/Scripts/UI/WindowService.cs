﻿using App.Scripts.ResourcesManagement;
using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;

namespace Assets.App.Scripts.UI
{
    public sealed class WindowService
    {
        private readonly Canvas _canvas;
        private readonly IAssetProvider _assetProvider;
        private readonly PrefabFactory<WindowBase> _windowPrefabFactory;

        public WindowService(Canvas canvas, IAssetProvider assetProvider, PrefabFactory<WindowBase> windowPrefabFactory)
        {
            _canvas = canvas;
            _assetProvider = assetProvider;
            _windowPrefabFactory = windowPrefabFactory;
        }

        public async UniTask<T> ShowWindow<T>(string assetKey, CancellationToken cancellationToken) where T : MonoBehaviour
        {
            var window = await _windowPrefabFactory.Create(assetKey, _canvas.transform, cancellationToken);
            window.transform.localPosition = Vector3.zero;

            return window as T;
        }

        public void HideWindow(MonoBehaviour window)
        {
            _assetProvider.ReleaseInstance(window.gameObject);
        }
    }
}