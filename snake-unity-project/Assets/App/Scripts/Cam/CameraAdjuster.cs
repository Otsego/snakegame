﻿using App.Scripts.Field.Model;
using JetBrains.Annotations;
using UnityEngine;

namespace App.Scripts.Cam
{
    [UsedImplicitly]
    public sealed class CameraAdjuster
    {
        private readonly FieldModel _fieldModel;

        public CameraAdjuster(FieldModel fieldModel)
        {
            _fieldModel = fieldModel;
        }

        public void AdjustCamera()
        {
            CenterCamera();
            SetOrthoSize();
        }

        private void CenterCamera()
        {
            var position = new Vector3(_fieldModel.Width / 2f - 0.5f, _fieldModel.Height / 2f - 0.5f, -100f);
            Camera.main.transform.position = position;
        }

        private void SetOrthoSize()
        {
            var screenRatio = (float)Screen.width / (float)Screen.height;
            var fieldRatio = (float)_fieldModel.Width / (float)_fieldModel.Height;
            var heightOrtho = _fieldModel.Height / 2f;
            var widthOrtho = (fieldRatio / screenRatio) * heightOrtho;

            Camera.main.orthographicSize = widthOrtho > heightOrtho ? widthOrtho : heightOrtho;
        }
    }
}