﻿using UnityEngine;

namespace App.Scripts.Ending.Signals
{
    public sealed class GameEnded
    {
        public readonly bool IsVictory;

        public GameEnded(bool isVictory)
        {
            Debug.Log($"Game ended. Victory: {isVictory}");

            IsVictory = isVictory;
        }
    }
}