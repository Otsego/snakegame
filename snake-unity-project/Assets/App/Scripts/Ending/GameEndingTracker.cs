﻿using App.Scripts.Ending.Signals;
using App.Scripts.Food.Signals;
using App.Scripts.Snake.Signals;
using JetBrains.Annotations;
using System;
using UniRx;

namespace Assets.App.Scripts.Ending
{
    [UsedImplicitly]
    public sealed class GameEndingTracker : IDisposable
    {
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        public GameEndingTracker()
        {
            MessageBroker
                .Default
                .Receive<FreeSpaceForFoodEnded>()
                .Subscribe(_ => MessageBroker.Default.Publish(new GameEnded(true)))
                .AddTo(_compositeDisposable);

            MessageBroker
                .Default
                .Receive<SnakeCollidedItself>()
                .Subscribe(_ => MessageBroker.Default.Publish(new GameEnded(false)))
                .AddTo(_compositeDisposable);
        }

        public void Dispose()
        {
            _compositeDisposable?.Dispose();
        }
    }
}