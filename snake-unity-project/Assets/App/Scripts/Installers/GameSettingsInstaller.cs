using App.Scripts.Field;
using App.Scripts.Snake.Settings;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "GameSettingsInstaller", menuName = "Installers/GameSettingsInstaller")]
public sealed class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
{
    public FieldSettings _fieldSettings;
    public SnakeSettings _snakeSettings;

    public override void InstallBindings()
    {
        Container.BindInstances(_fieldSettings, _snakeSettings);
    }
}