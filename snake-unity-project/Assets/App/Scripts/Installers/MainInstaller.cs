using System;
using App.Scripts.Cam;
using App.Scripts.Direction;
using App.Scripts.Field.Model;
using App.Scripts.Food;
using App.Scripts.Food.Model;
using App.Scripts.Food.Visualizer;
using App.Scripts.Pause;
using App.Scripts.Snake;
using App.Scripts.Snake.Model;
using App.Scripts.Field.Visualizer;
using App.Scripts.ResourcesManagement;
using App.Scripts.Snake.Visualizer;
using Framework.FSM;
using Framework.FSM.States;
using UnityEngine;
using Zenject;
using App.Scripts.Scores;
using Assets.App.Scripts.UI;
using Assets.App.Scripts;
using App.Scripts.StorageService;
using System.Threading;
using Cysharp.Threading.Tasks;
using Assets.App.Scripts.Ending;
using App.Scripts.Field;
using Assets.App.Scripts.Curtain;

namespace App.Scripts.Installers
{
    public sealed class MainInstaller : MonoInstaller
    {
        [SerializeField] private ResourcesStorage _resourcesStorage;
        [SerializeField] private FoodView _foodViewPrefab;
        [SerializeField] private SnakeSegmentView _snakeSegmentViewPrefab;
        [SerializeField] private Canvas _mainCanvas;
        [SerializeField] private CurtainView _curtain;
        
        public override void InstallBindings()
        {
            Container.Bind<AppService>().AsSingle();
            Container.Bind<ResourcesStorage>().FromInstance(_resourcesStorage);
            
            BindStateMachine();
            BindGameField();
            BindSnake();
            BindFood();
            BindWindows();

            Container.Bind<CameraAdjuster>().AsSingle();
            Container.Bind<Canvas>().FromInstance(_mainCanvas);
            Container.BindInterfacesAndSelfTo<DirectionProvider>().AsSingle().NonLazy();
            Container.Bind<PauseService>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<ScoreService>().AsSingle().NonLazy();
            Container.Bind<IAssetProvider>().To<AddressablesAssetProvider>().AsSingle();
            Container.Bind<IStorageService>().To<PlayerPrefsStorage>().AsSingle();
            Container.BindInterfacesAndSelfTo<GameEndingTracker>().AsSingle().NonLazy();
            Container.Bind<CurtainView>().FromInstance(_curtain);
        }

        private void BindStateMachine()
        {
            Container.Bind(typeof(InjectableStateMachine<>)).AsTransient();
            Container.Bind<InjectableDefaultStateMachine>().AsTransient();
            Container.BindFactory<Type, IState, StateFactory>().FromFactory<CustomStateFactory>();
        }

        private void BindSnake()
        {
            Container.Bind<SnakeModel>().AsSingle();
            Container.BindFactory<uint, Vector2Int, Vector2Int, SnakeSegment, SnakeSegmentFactory>().AsSingle();
            Container.BindInterfacesAndSelfTo<SnakeMoveMaker>().AsSingle();
            Container.BindInterfacesAndSelfTo<SnakeVisualizer>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<SnakeEnlarger>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<SnakeSelfCollisionWatcher>().AsSingle().NonLazy();
            Container.BindFactory<SnakeSegmentView, SnakeSegmentView.Factory>().FromComponentInNewPrefab(_snakeSegmentViewPrefab);
        }

        private void BindGameField()
        {
            Container.Bind<FieldVisualizer>().AsSingle();
            Container.Bind<FieldModel>().AsSingle();
            Container.Bind<FieldFreePlaceProvider>().AsSingle();
        }

        private void BindFood()
        {
            Container.Bind<FoodModel>().AsSingle();
            Container.BindInterfacesAndSelfTo<FoodConsumptionEvaluator>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<FoodSpawner>().AsSingle();
            Container.BindInterfacesAndSelfTo<FoodVisualizer>().AsSingle().NonLazy();
            Container.BindMemoryPool<FoodView, FoodView.Pool>()
                .WithInitialSize(1)
                .FromComponentInNewPrefab(_foodViewPrefab)
                .UnderTransformGroup("Food Pool");
        }

        private void BindWindows()
        {
            Container.Bind<WindowService>().AsSingle();

            Container.BindFactory<string, Transform, CancellationToken, UniTask<WindowBase>, PrefabFactory<WindowBase>>().FromFactory<AsyncPrefabFactory<WindowBase>>();
        }
    }
}