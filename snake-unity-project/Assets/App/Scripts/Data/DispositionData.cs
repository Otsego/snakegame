using UnityEngine;

namespace App.Scripts.Data
{
    public readonly struct DispositionData
    {
        public readonly Vector2Int Position;
        public readonly Vector2Int Direction;

        public DispositionData(Vector2Int position, Vector2Int direction)
        {
            Position = position;
            Direction = direction;
        }
    }
}