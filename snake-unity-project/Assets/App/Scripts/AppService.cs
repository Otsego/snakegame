﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.App.Scripts
{
    public sealed class AppService
    {
        public void Restart()
        {
            SceneManager.LoadScene("Main");
        }

        public void Quit()
        {
            Application.Quit();
        }
    }
}