﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Scripts.Data;
using App.Scripts.Direction;
using App.Scripts.Field.Model;
using App.Scripts.Pause;
using App.Scripts.Snake.Model;
using JetBrains.Annotations;
using UniRx;
using UnityEngine;

namespace App.Scripts.Snake
{
    [UsedImplicitly]
    public sealed class SnakeMoveMaker : IPausable, IDisposable
    {
        public readonly ReactiveProperty<IList<DispositionData>> SegmentDispositions = new ReactiveProperty<IList<DispositionData>>(new List<DispositionData>());

        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private readonly FieldModel _fieldModel;
        private readonly SnakeModel _snakeModel;
        private readonly DirectionProvider _directionProvider;
        
        private Vector2Int _startPositionOnField;
        private float _speed;
        private bool _isPaused;
        
        public SnakeMoveMaker(
            SnakeModel snakeModel,
            FieldModel fieldModel,
            DirectionProvider directionProvider)
        {
            _snakeModel = snakeModel;
            _fieldModel = fieldModel;
            _directionProvider = directionProvider;
        }

        public void Run(Vector2Int startPositionOnField, float speed)
        {
            _startPositionOnField = startPositionOnField;
            _speed = speed;

            Observable
                .Interval(TimeSpan.FromSeconds(1f / _speed))
                .Subscribe(_ => Move())
                .AddTo(_compositeDisposable);

            Move();
        }

        private void Move()
        {
            if (_isPaused)
            {
                return;
            }
            
            _snakeModel.ChangeDirection(_directionProvider.LatestDirection.Value);
            
            SegmentDispositions.Value = _snakeModel
                .Move()
                .Select(data => new DispositionData(ConvertSnakeSegmentToFieldPosition(data.Position), data.Direction))
                .ToList();
        }

        private Vector2Int ConvertSnakeSegmentToFieldPosition(Vector2Int segmentPosition)
        {
            var position = segmentPosition + _startPositionOnField;

            position.x %= _fieldModel.Width;

            if (position.x < 0)
            {
                position.x += _fieldModel.Width;
            }

            position.y %= _fieldModel.Height;

            if (position.y < 0)
            {
                position.y += _fieldModel.Height;
            }

            return position;
        }

        public void Dispose()
        {
            _compositeDisposable?.Dispose();
        }

        void IPausable.SetPauseStatus(bool isPaused)
        {
            _isPaused = isPaused;
        }
    }
}