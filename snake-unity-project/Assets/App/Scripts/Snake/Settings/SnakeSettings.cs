﻿using UnityEngine;

namespace App.Scripts.Snake.Settings
{
    public sealed class SnakeSettings : ScriptableObject
    {
        public SnakeStartSettings StartSettings => _startSettings;
        public float Speed => _speed;

        [SerializeField] private SnakeStartSettings _startSettings;
        [SerializeField] private float _speed;
    }
}