using System;
using UnityEngine;

namespace App.Scripts.Snake.Settings
{
    [Serializable]
    public sealed class SnakeStartSettings
    {
        public uint Segments => _segments;
        public Vector2Int Direction => _direction;
        public Vector2Int StartPosition => _startPosition;

        [SerializeField] private uint _segments;
        [SerializeField] private Vector2Int _direction;
        [SerializeField] private Vector2Int _startPosition;

        public SnakeStartSettings(uint segments, Vector2Int direction, Vector2Int startPosition)
        {
            _segments = segments;
            _direction = direction;
            _startPosition = startPosition;
        }
    }
}