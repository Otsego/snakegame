﻿using System;
using App.Scripts.Food.Model;
using App.Scripts.Snake.Model;
using JetBrains.Annotations;
using UniRx;

namespace App.Scripts.Snake
{
    [UsedImplicitly]
    public sealed class SnakeEnlarger : IDisposable
    {
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        public SnakeEnlarger(SnakeModel snakeModel, FoodModel foodModel)
        {
            foodModel
                .FoodPositions
                .ObserveRemove()
                .Subscribe(_ => snakeModel.AddSegment())
                .AddTo(_compositeDisposable);
        }

        public void Dispose()
        {
            _compositeDisposable?.Dispose();
        }
    }
}