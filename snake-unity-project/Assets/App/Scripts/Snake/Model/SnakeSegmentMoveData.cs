using UnityEngine;

namespace App.Scripts.Snake.Model
{
    public readonly struct SnakeSegmentMoveData
    {
        public readonly uint SegmentNumber;
        public readonly Vector2Int Position;
        public readonly Vector2Int Direction;

        public SnakeSegmentMoveData(
            uint segmentNumber,
            Vector2Int position,
            Vector2Int direction)
        {
            SegmentNumber = segmentNumber;
            Position = position;
            Direction = direction;
        }
    }
}