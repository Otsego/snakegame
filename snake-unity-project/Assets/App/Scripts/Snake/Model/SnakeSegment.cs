using System.Collections.Generic;
using Framework;
using JetBrains.Annotations;
using UnityEngine;

namespace App.Scripts.Snake.Model
{
    [UsedImplicitly]
    public sealed class SnakeSegment
    {
        private Maybe<SnakeSegment> _nextSegment;
        private Maybe<Vector2Int> _pendingDirection;
        
        public uint SegmentNumber { get; }
        public Vector2Int Direction { get; private set; }
        public Vector2Int Position { get; private set; }

        public SnakeSegment(uint segmentNumber, Vector2Int direction, Vector2Int position)
        {
            SegmentNumber = segmentNumber;
            Direction = direction;
            Position = position;
        }

        public void Move(IList<SnakeSegmentMoveData> segmentsMoves)
        {
            _pendingDirection.Do(direction => Direction = direction);

            Position += Direction;
            segmentsMoves.Add(new SnakeSegmentMoveData(SegmentNumber, Position, Direction));
            
            _nextSegment.Do(nextSegment =>
            {
                nextSegment.Move(segmentsMoves);
                _pendingDirection.Do(nextSegment.SetNewDirection);
            });
            
            _pendingDirection = Maybe<Vector2Int>.None;
        }

        public void SetNewDirection(Vector2Int direction)
        {
            _pendingDirection = direction.AsMaybe();
        }

        public void AttachSegment(SnakeSegment segment)
        {
            _nextSegment = segment.AsMaybe();
        }
    }
}