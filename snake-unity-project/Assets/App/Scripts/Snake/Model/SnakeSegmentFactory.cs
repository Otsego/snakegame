using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace App.Scripts.Snake.Model
{
    [UsedImplicitly]
    public sealed class SnakeSegmentFactory : PlaceholderFactory<uint, Vector2Int, Vector2Int, SnakeSegment>
    { }
}