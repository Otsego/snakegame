using System;
using System.Collections.Generic;
using App.Scripts.Snake.Settings;
using JetBrains.Annotations;
using UnityEngine;

namespace App.Scripts.Snake.Model
{
    [UsedImplicitly]
    public sealed class SnakeModel
    {
        private readonly SnakeSegmentFactory _snakeSegmentFactory;

        public uint Length => _tail.SegmentNumber + 1;
        
        private SnakeSegment _head;
        private SnakeSegment _tail;
        
        private Vector2Int _currentDirection;

        public SnakeModel(SnakeSegmentFactory snakeSegmentFactory)
        {
            _snakeSegmentFactory = snakeSegmentFactory;
        }

        public void Create(SnakeStartSettings settings)
        {
            if (settings.Segments < 1)
            {
                throw new ArgumentException("Invalid number of segments. Must be greater then 0");
            }

            _currentDirection = settings.Direction;
            _head = _tail = _snakeSegmentFactory.Create(0, settings.Direction, Vector2Int.zero);

            for (var i = 0; i < settings.Segments - 1; i++)
            {
                AddSegment();
            }
        }

        public void AddSegment()
        {
            var segment = _snakeSegmentFactory.Create(
                _tail.SegmentNumber + 1,
                _tail.Direction,
                _tail.Position - _tail.Direction);
            
            _tail.AttachSegment(segment);
            _tail = segment;
        }

        public IReadOnlyList<SnakeSegmentMoveData> Move()
        {
            var moves = new List<SnakeSegmentMoveData>();
            
            _head.Move(moves);

            return moves;
        }

        public void ChangeDirection(Vector2Int newDirection)
        {
            if (!IsNewDirectionAppropriate(newDirection))
            {
                return;
            }

            _currentDirection = newDirection;
            _head.SetNewDirection(newDirection);
        }

        private bool IsNewDirectionAppropriate(Vector2Int newDirection)
        {
            return newDirection != _currentDirection && newDirection != -_currentDirection;
        }
    }
}