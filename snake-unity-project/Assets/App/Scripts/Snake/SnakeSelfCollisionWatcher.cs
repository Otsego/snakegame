﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Scripts.Data;
using App.Scripts.Snake.Signals;
using JetBrains.Annotations;
using UniRx;

namespace App.Scripts.Snake
{
    [UsedImplicitly]
    public sealed class SnakeSelfCollisionWatcher : IDisposable
    {
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        public SnakeSelfCollisionWatcher(SnakeMoveMaker snakeMoveMaker)
        {
            snakeMoveMaker
                .SegmentDispositions
                .Subscribe(CheckSnakeSelfCollisions)
                .AddTo(_compositeDisposable);
        }

        private void CheckSnakeSelfCollisions(IList<DispositionData> dispositions)
        {
            var allPositions = dispositions.Select(disposition => disposition.Position).ToList();

            if (allPositions.Count != allPositions.Distinct().Count())
            {
                MessageBroker.Default.Publish(new SnakeCollidedItself());
            }
        }

        public void Dispose()
        {
            _compositeDisposable?.Dispose();
        }
    }
}