using App.Scripts.Snake.Settings;
using Framework;
using UnityEngine;
using Zenject;

namespace App.Scripts.Snake.Visualizer
{
    public sealed class SnakeSegmentView : MonoBehaviour
    {
        [Inject] private readonly SnakeSettings _snakeSettings;

        public Transform Transform => _transform;

        [SerializeField] SpriteRenderer _spriteRenderer;
        [SerializeField] Transform _transform;

        [Header("Colors")]
        [SerializeField] private Color _headColor;
        [SerializeField] private Color _aColor;
        [SerializeField] private Color _bColor;

        private Maybe<Vector3> _maybeDirection;

        private void Update()
        {
            if (_maybeDirection.HasValue)
            {
                _transform.localPosition += _maybeDirection.Value * _snakeSettings.Speed * Time.deltaTime;
            }
        }

        public void SetDirection(Vector2Int direction)
        {
            _maybeDirection = new Vector3(direction.x, direction.y, _transform.localPosition.z).AsMaybe();
        }

        public void SetHeadAppearance()
        {
            _spriteRenderer.color = _headColor;
            _spriteRenderer.sortingOrder = 1;
        }

        public void SetAppearanceA()
        {
            _spriteRenderer.color = _aColor;
            _spriteRenderer.sortingOrder = 0;
        }

        public void SetAppearanceB()
        {
            _spriteRenderer.color = _bColor;
            _spriteRenderer.sortingOrder = 0;
        }

        public sealed class Factory : PlaceholderFactory<SnakeSegmentView>
        {
        }
    }
}