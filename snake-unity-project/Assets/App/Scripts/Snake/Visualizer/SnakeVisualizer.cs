using System;
using System.Collections.Generic;
using App.Scripts.Data;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using UniRx;
using UnityEngine;

namespace App.Scripts.Snake.Visualizer
{
    [UsedImplicitly]
    public sealed class SnakeVisualizer : IDisposable
    {
        private SnakeSegmentView.Factory _snakeSegmentViewFactory;

        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private readonly List<SnakeSegmentView> _segmentViews = new List<SnakeSegmentView>();

        private Transform _layer;

        public SnakeVisualizer(
            SnakeMoveMaker moveMaker,
            SnakeSegmentView.Factory snakeSegmentViewFactory)
        {
            _snakeSegmentViewFactory = snakeSegmentViewFactory;
            CreateLayer();

            moveMaker
                .SegmentDispositions
                .Where(positions => positions != null)
                .Subscribe(Visualize)
                .AddTo(_compositeDisposable);
        }

        private void CreateLayer()
        {
            _layer = new GameObject("Snake Layer").transform;
            _layer.position = new Vector3(0f, 0f, -1f);
        }

        private void Visualize(IList<DispositionData> dispositions)
        {
            for (var i = 0; i < dispositions.Count; i++)
            {
                var view = GetSegmentView(i);
                var position = dispositions[i].Position - dispositions[i].Direction;
                view.Transform.localPosition = new Vector3(position.x, position.y, 0f);
                SetSegmentViewColor(view, i);
                view.SetDirection(dispositions[i].Direction);
            }
        }

        private void SetSegmentViewColor(SnakeSegmentView view, int number)
        {
            if (number == 0)
            {
                view.SetHeadAppearance();

                return;
            }

            if (number % 2 == 0)
            {
                view.SetAppearanceB();

                return;
            }

            view.SetAppearanceA();
        }

        private SnakeSegmentView GetSegmentView(int i)
        {
            if (i >= _segmentViews.Count)
            {
                var segmentView = _snakeSegmentViewFactory.Create();
                segmentView.Transform.SetParent(_layer, false);
                _segmentViews.Add(segmentView);
            }

            return _segmentViews[i];
        }

        public void Dispose()
        {
            _compositeDisposable?.Dispose();
        }
    }
}