﻿namespace App.Scripts.StorageService
{
    public interface IStorageService
    {
        void Set<T>(string key, T value);
        T Get<T>(string key);
        void Save();
    }
}