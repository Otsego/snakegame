﻿using Framework;
using System;
using UnityEngine;

namespace App.Scripts.StorageService
{
    /// <summary>
    /// Warning: doesn't save Decimals
    /// </summary>
    public sealed class PlayerPrefsStorage : IStorageService
    {
        public T Get<T>(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Storage key mustn't be empty");
            }

            return PlayerPrefs.GetString(key)
                .AsMaybe()
                .Where(json => !string.IsNullOrWhiteSpace(json))
                .Select(json => JsonUtility.FromJson<Wrapper<T>>(json).Value)
                .Else(() => default);
        }

        public void Set<T>(string key, T value)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Storage key mustn't be empty");
            }

            var json = JsonUtility.ToJson(new Wrapper<T>(value));

            PlayerPrefs.SetString(key, json);
        }

        public void Save()
        {
            Debug.Log("Storage saved");

            PlayerPrefs.Save();
        }

        [Serializable]
        private struct Wrapper<T>
        {
            public T Value;

            public Wrapper(T value)
            {
                Value = value;
            }
        }
    }
}