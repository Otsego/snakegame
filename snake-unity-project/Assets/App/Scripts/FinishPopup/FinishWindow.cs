using App.Scripts.Scores;
using Assets.App.Scripts;
using Assets.App.Scripts.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public sealed class FinishWindow : WindowBase
{
    [Inject] private readonly AppService _appService;
    [Inject] private readonly ScoreService _scoreService;

    [SerializeField] private TMP_Text _currentGameScore;
    [SerializeField] private TMP_Text _recordScore;
    [SerializeField] private GameObject _victoryLabel;
    [SerializeField] private GameObject _newRecordLabel;
    [SerializeField] private Button _restartButton;
    [SerializeField] private Button _quitButton;

    private void Start()
    {
        Debug.Log("Finish Window showed");

        _currentGameScore.text = $"Score: {_scoreService.Score.Value}";
        _recordScore.text = $"Record: {_scoreService.Record}";
        _newRecordLabel.SetActive(_scoreService.IsRecordBroken);

        _restartButton.onClick.AddListener(() => { _appService.Restart(); });

        SetQuitButton();
    }

    public void SetContext(bool isVictory)
    {
        _victoryLabel.SetActive(isVictory);
    }

    private void SetQuitButton()
    {
#if UNITY_WEBGL
        _quitButton.gameObject.SetActive(false);
#else
        _quitButton.onClick.AddListener(() => { _appService.Quit(); });
#endif
    }
}