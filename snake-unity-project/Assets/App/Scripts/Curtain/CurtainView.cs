using UnityEngine;

namespace Assets.App.Scripts.Curtain
{
    public sealed class CurtainView : MonoBehaviour
    {
        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}