using App.Scripts.MainSequence;
using Cysharp.Threading.Tasks;
using Framework.FSM;
using UnityEngine;
using Zenject;
using UniRx;

namespace App.Scripts
{
    public sealed class EntryPoint : MonoBehaviour
    {
        [Inject] private readonly InjectableDefaultStateMachine _mainStateMachine;
        
        private async UniTaskVoid Start()
        {
            _mainStateMachine.AddTo(this);
            await _mainStateMachine.Activate<AppEntryPointState>();
        }
    }
}