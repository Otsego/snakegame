using App.Scripts.Pause;
using JetBrains.Annotations;
using UniRx;
using UnityEngine;
using Zenject;

namespace App.Scripts.Direction
{
    [UsedImplicitly]
    public sealed class DirectionProvider : IPausable, ITickable
    {
        public readonly ReactiveProperty<Vector2Int> LatestDirection = new ReactiveProperty<Vector2Int>();
      
        private bool _isPaused;

        public void SetInitialDirection(Vector2Int direction)
        {
            LatestDirection.Value = direction;
        }
        
        public void Tick()
        {
            if (_isPaused)
            {
                return;
            }
            
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                SetNewDirection(Vector2Int.up);
                
                return;
            }
            
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                SetNewDirection(Vector2Int.down);
                
                return;
            }
            
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                SetNewDirection(Vector2Int.left);
                
                return;
            }
            
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                SetNewDirection(Vector2Int.right);
                
                return;
            }
        }

        private void SetNewDirection(Vector2Int direction)
        {
            if (LatestDirection.Value != direction)
            {
                LatestDirection.Value = direction;
            }
        }
        
        void IPausable.SetPauseStatus(bool isPaused)
        {
            _isPaused = isPaused;
        }
    }
}