﻿using App.Scripts.Ending.Signals;
using App.Scripts.Pause;
using App.Scripts.StorageService;
using Assets.App.Scripts.Curtain;
using Assets.App.Scripts.UI;
using Cysharp.Threading.Tasks;
using Framework.FSM.States;
using JetBrains.Annotations;
using System.Threading;
using UniRx;
using UnityEngine;
using Zenject;

namespace App.Scripts.MainSequence
{
    [UsedImplicitly]
    public sealed class GamePlayingState : InjectableDefaultState
    {
        [Inject] private readonly WindowService _windowService;
        [Inject] private readonly PauseService _pauseService;
        [Inject] private readonly IStorageService _storageService;
        [Inject] private readonly CurtainView _curtainView;

        private CancellationToken _cancellationToken;

        public async override UniTask Enter()
        {
            var tokenSource = new CancellationTokenSource();
            _compositeDisposable.Add(Disposable.Create(tokenSource.Cancel));
            _cancellationToken = tokenSource.Token;

            MessageBroker
                .Default
                .Receive<GameEnded>()
                .Subscribe(OnGameEnded)
                .AddTo(_compositeDisposable);

            _curtainView.Hide();

            await UniTask.NextFrame();
        }

        private void OnGameEnded(GameEnded gameEndedSignal)
        {
            Debug.Log($"{nameof(GamePlayingState)}: {nameof(OnGameEnded)}");

            _pauseService.SetPauseStatus(true);
            _storageService.Save();

            _windowService
                .ShowWindow<FinishWindow>("FinishWindow", _cancellationToken)
                .ContinueWith(window => window.SetContext(gameEndedSignal.IsVictory))
                .Forget();
        }
    }
}