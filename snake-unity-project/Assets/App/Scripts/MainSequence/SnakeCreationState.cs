﻿using App.Scripts.Direction;
using App.Scripts.Snake;
using App.Scripts.Snake.Model;
using App.Scripts.Snake.Settings;
using Cysharp.Threading.Tasks;
using Framework.FSM.States;
using JetBrains.Annotations;
using Zenject;

namespace App.Scripts.MainSequence
{
    [UsedImplicitly]
    public sealed class SnakeCreationState : InjectableDefaultState
    {
        [Inject] private readonly SnakeSettings _snakeSettings;
        [Inject] private readonly SnakeModel _snakeModel;
        [Inject] private readonly SnakeMoveMaker _snakeMoveMaker;
        [Inject] private readonly DirectionProvider _directionProvider;

        public override async UniTask Enter()
        {
            _directionProvider.SetInitialDirection(_snakeSettings.StartSettings.Direction);
            _snakeModel.Create(_snakeSettings.StartSettings);
            _snakeMoveMaker.Run(_snakeSettings.StartSettings.StartPosition, _snakeSettings.Speed);

            await UniTask.NextFrame();
            await StateMachine.Activate<ServicesStartState>();
        }
    }
}