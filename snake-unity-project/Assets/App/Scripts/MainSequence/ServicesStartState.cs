﻿using App.Scripts.Cam;
using App.Scripts.Food;
using App.Scripts.Scores;
using Cysharp.Threading.Tasks;
using Framework.FSM.States;
using JetBrains.Annotations;
using Zenject;

namespace App.Scripts.MainSequence
{
    [UsedImplicitly]
    public sealed class ServicesStartState : InjectableDefaultState
    {
        [Inject] private readonly FoodSpawner _foodSpawner;
        [Inject] private readonly CameraAdjuster _cameraPositioner;
        [Inject] private readonly ScoreService _scoreService;

        public override async UniTask Enter()
        {
            _foodSpawner.Start();
            _cameraPositioner.AdjustCamera();
            _scoreService.Start();

            await UniTask.NextFrame();
            await StateMachine.Activate<GamePlayingState>();
        }
    }
}