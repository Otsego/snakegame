﻿using App.Scripts.Field;
using App.Scripts.Field.Model;
using App.Scripts.Field.Visualizer;
using Cysharp.Threading.Tasks;
using Framework.FSM.States;
using JetBrains.Annotations;
using Zenject;

namespace App.Scripts.MainSequence
{
    [UsedImplicitly]
    public sealed class GameFieldCreationState : InjectableDefaultState
    {
        [Inject] private readonly FieldSettings _fieldSettings;
        [Inject] private readonly FieldModel _fieldModel;
        [Inject] private readonly FieldVisualizer _fieldVisualizer;

        public override async UniTask Enter()
        {
            _fieldModel.SetDimensions(_fieldSettings.Width, _fieldSettings.Height);
            _fieldVisualizer.ShowField();

            await UniTask.DelayFrame(2);
            await StateMachine.Activate<SnakeCreationState>();
        }
    }
}