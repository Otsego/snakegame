using Assets.App.Scripts.Curtain;
using Cysharp.Threading.Tasks;
using Framework.FSM.States;
using JetBrains.Annotations;
using Zenject;

namespace App.Scripts.MainSequence
{
    [UsedImplicitly]
    public sealed class AppEntryPointState : InjectableDefaultState
    {
        [Inject] private readonly CurtainView _curtainView;

        public override async UniTask Enter()
        {
            _curtainView.Show();

            await UniTask.DelayFrame(3);
            await StateMachine.Activate<GameFieldCreationState>();
        }
    }
}