namespace App.Scripts.Field.Model
{
    public sealed class FieldModel
    {
        public int Width { get; private set; }
        public int Height { get; private set; }

        public void SetDimensions(uint width, uint height)
        {
            Width = (int) width;
            Height = (int) height;
        }
    }
}