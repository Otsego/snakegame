using App.Scripts.Field.Model;
using App.Scripts.Field.Visualizer;
using App.Scripts.ResourcesManagement;
using JetBrains.Annotations;
using UnityEngine;

namespace App.Scripts.Field.Visualizer
{
    [UsedImplicitly]
    public sealed class FieldVisualizer
    {
        private readonly FieldModel _fieldModel;
        private readonly ResourcesStorage _resourcesStorage;

        public FieldVisualizer(FieldModel fieldModel, ResourcesStorage resourcesStorage)
        {
            _fieldModel = fieldModel;
            _resourcesStorage = resourcesStorage;
        }

        public void ShowField()
        {
            var position = new Vector3(_fieldModel.Width / 2f - 0.5f, _fieldModel.Height / 2f - 0.5f, 0f);
            var fieldView = Object.Instantiate(_resourcesStorage.FieldViewPrefab, position, Quaternion.identity);
            fieldView.GetComponent<FieldView>().Initialize(new Vector3(_fieldModel.Width, _fieldModel.Height, 1f));
        }
    }
}