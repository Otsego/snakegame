using UnityEngine;

namespace App.Scripts.Field.Visualizer
{
    public sealed class FieldView : MonoBehaviour
    {
        [SerializeField] private Material _material;

        public void Initialize(Vector3 size)
        {
            transform.localScale = size;
            _material.SetTextureScale("_MainTex", size);
        }
    }
}