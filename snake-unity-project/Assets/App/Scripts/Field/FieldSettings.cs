using UnityEngine;

namespace App.Scripts.Field
{
    public sealed class FieldSettings : ScriptableObject
    {
        public uint Width => _width;
        public uint Height => _height;

        [SerializeField] private uint _width;
        [SerializeField] private uint _height;
    }
}