﻿using App.Scripts.Field.Model;
using App.Scripts.Snake;
using Framework;
using JetBrains.Annotations;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace App.Scripts.Field
{
    [UsedImplicitly]
    public sealed class FieldFreePlaceProvider
    {
        private readonly FieldModel _fieldModel;
        private readonly SnakeMoveMaker _snakeMoveMaker;

        public FieldFreePlaceProvider(FieldModel fieldModel, SnakeMoveMaker snakeMoveMaker)
        {
            _fieldModel = fieldModel;
            _snakeMoveMaker = snakeMoveMaker;
        }

        public Maybe<Vector2Int> GetRandomFreePosition()
        {
            var freePositions = new List<Vector2Int>();

            var snakeHead = _snakeMoveMaker.SegmentDispositions.Value.FirstOrDefault();
            var snakeHeadNextPosition = snakeHead.Position + snakeHead.Direction;

            for (var i = 0; i < _fieldModel.Width; i++)
            {
                for (var j = 0; j < _fieldModel.Height; j++)
                {
                    var position = new Vector2Int(i, j);

                    if (position == snakeHeadNextPosition)
                    {
                        continue;
                    }

                    var maybeFreePosition = position.AsMaybe();

                    foreach (var disposition in _snakeMoveMaker.SegmentDispositions.Value)
                    {
                        if (position == disposition.Position)
                        {
                            maybeFreePosition = Maybe<Vector2Int>.None;
                            break;
                        }
                    }

                    maybeFreePosition.Do(pos => freePositions.Add(pos));
                }
            }

            if (freePositions.Count == 0)
            {
                return Maybe<Vector2Int>.None;
            }

            var randomIndex = UnityEngine.Random.Range(0, freePositions.Count);
            return freePositions[randomIndex].AsMaybe();
        }
    }
}