﻿using Cysharp.Threading.Tasks;
using Framework.FSM.States;
using JetBrains.Annotations;
using Zenject;

namespace Framework.FSM
{
    [UsedImplicitly]
    public class InjectableStateMachine<TContext> : StateMachine<TContext>
    {
        [Inject] private readonly StateFactory _stateFactory;
        
        public async UniTask Activate<TState, TArgument>(TArgument argument)
            where TState : StateBase<TContext, TArgument>
        {
            var state = _stateFactory.Create(typeof(TState)) as TState;

            await Activate(state, argument);
        }

        public async UniTask Activate<TState>() where TState : StateBase<TContext, DefaultArgument>
        {
            var state = _stateFactory.Create(typeof(TState)) as TState;

            await Activate(state, default);
        }
    }
}