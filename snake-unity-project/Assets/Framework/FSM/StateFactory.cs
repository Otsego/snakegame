﻿using System;
using Framework.FSM.States;
using JetBrains.Annotations;
using Zenject;

namespace Framework.FSM
{
    [UsedImplicitly]
    public class StateFactory : PlaceholderFactory<Type, IState>{}
    
    [UsedImplicitly]
    public sealed class CustomStateFactory : IFactory<Type, IState>
    {
        private readonly DiContainer _container;
        
        public CustomStateFactory(DiContainer container)
        {
            _container = container;
        }
        
        public IState Create(Type stateType)
        {
            var state = _container.Instantiate(stateType);

            return state as IState;
        }
    }
}