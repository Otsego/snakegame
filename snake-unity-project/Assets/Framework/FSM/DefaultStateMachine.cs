﻿using JetBrains.Annotations;
using Framework.FSM.States;

namespace Framework.FSM
{
    [UsedImplicitly]
    public sealed class DefaultStateMachine : StateMachine<DefaultContext>
    {
        
    }
}