﻿using Cysharp.Threading.Tasks;
using UniRx;

namespace Framework.FSM.States
{
    public abstract class StateBase<TContext, TArgument> : IState
    {
        protected StateMachine<TContext> StateMachine { get; private set; }
        protected TContext Context { get; private set; }
        protected TArgument Argument { get; private set; }
        
        protected readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        public void Initialize(StateMachine<TContext> stateMachine, TContext context, TArgument argument)
        {
            Initialize(stateMachine, context);
            Argument = argument;
        }
        
        public void Initialize(StateMachine<TContext> stateMachine, TContext context)
        {
            StateMachine = stateMachine;
            Context = context;
        }

        public virtual UniTask Enter()
        {
            return UniTask.CompletedTask;
        }

        public virtual UniTask Exit()
        {
            return UniTask.CompletedTask;
        }
        
        public void Dispose()
        {
            _compositeDisposable.Dispose();
        }
    }
}