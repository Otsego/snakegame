﻿namespace Framework.FSM.States
{
    public abstract class NoArgState<TContext> : StateBase<TContext, DefaultArgument>
    {
        
    }
}