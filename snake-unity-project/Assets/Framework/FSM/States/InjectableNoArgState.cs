﻿namespace Framework.FSM.States
{
    public abstract class InjectableNoArgState<TContext> : InjectableStateBase<TContext, DefaultArgument>
    {
        
    }
}