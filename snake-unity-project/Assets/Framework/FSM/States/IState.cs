﻿using System;
using Cysharp.Threading.Tasks;

namespace Framework.FSM.States
{
    public interface IState : IDisposable
    {
        UniTask Enter();
        UniTask Exit();
    }
}