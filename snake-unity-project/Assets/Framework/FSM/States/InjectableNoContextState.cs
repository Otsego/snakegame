﻿namespace Framework.FSM.States
{
    public abstract class InjectableNoContextState<TArgument> : InjectableStateBase<DefaultContext, TArgument>
    {
        
    }
}