﻿namespace Framework.FSM.States
{
    public abstract class InjectableStateBase<TContext, TArgument> : StateBase<TContext, TArgument>
    {
        protected new InjectableDefaultStateMachine StateMachine => base.StateMachine as InjectableDefaultStateMachine;
    }
}