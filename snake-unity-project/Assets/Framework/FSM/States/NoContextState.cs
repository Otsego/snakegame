﻿namespace Framework.FSM.States
{
    public abstract class NoContextState<TArgument> : StateBase<DefaultContext, TArgument>
    {
        
    }
}