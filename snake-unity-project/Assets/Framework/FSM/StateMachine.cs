﻿using System;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Framework.FSM.States;

namespace Framework.FSM
{
    [UsedImplicitly]
    public class StateMachine<TContext> : IDisposable
    {
        public event Action Finished;
        public bool IsRunning { get; private set; } = true;
        
        private TContext _context;
        private IState _currentState;

        public void SetContext(TContext context)
        {
            if (_context != null)
            {
                throw new ArgumentException("You can set a context only once");
            }

            _context = context;
        }

        public async UniTask Activate<TArgument>(StateBase<TContext, TArgument> state, TArgument argument)
        {
            if (IsRunning)
            {
                await ExitCurrentState();
                await EnterNewState(state, argument);
            }
        }
        
        public async UniTask Activate<TArgument>(StateBase<TContext, TArgument> state)
        {
            await Activate(state, default);
        }

        public void Finish()
        {
            var onFinish = Finished;
            Dispose();
            onFinish?.Invoke();
        }

        private async UniTask ExitCurrentState()
        {
            if (_currentState != null)
            {
                await _currentState.Exit();
                DisposeCurrentState();
            }
        }

        private void DisposeCurrentState()
        {
            if (_currentState != null)
            {
                _currentState.Dispose();
                _currentState = null;
            }
        }

        public void Dispose()
        {
            IsRunning = false;
            DisposeCurrentState();
            Finished = null;
        }

        private async UniTask EnterNewState<TArgument>(StateBase<TContext, TArgument> state, TArgument argument)
        {
            _currentState = state;
            state.Initialize(this, _context, argument);
            await state.Enter();
        }
    }
}