﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Framework
{
    public struct Maybe<T> : IEquatable<Maybe<T>>
    {
        public static readonly Maybe<T> None = new Maybe<T>(default(T), false);

        private readonly T _value;

        public bool HasValue { get; }

        public T Value
        {
            get
            {
                if (!HasValue)
                {
                    throw new InvalidOperationException("Maybe value is empty.");
                }

                return _value;
            }
        }

        private Maybe(T value, bool hasValue)
        {
            _value = value;
            HasValue = hasValue;
        }

        public Maybe(T value) : this(value, true)
        {
            if (ReferenceEquals(null, value))
            {
                throw new ArgumentNullException(nameof(value));
            }
        }

        public TResult Match<TResult>(Func<T, TResult> valueProjection, Func<TResult> alternativeFunc)
        {
            return HasValue ? valueProjection(_value) : alternativeFunc();
        }

        public TResult Match<TResult>(Func<T, TResult> valueProjection, TResult alternative)
        {
            return Match(valueProjection, () => alternative);
        }

        public Maybe<TResult> Bind<TResult>(Func<T, Maybe<TResult>> projection)
        {
            return Match(projection, Maybe<TResult>.None);
        }

        public Maybe<TResult> Select<TResult>(Func<T, TResult> projection)
        {
            return Bind(value => projection(value).AsMaybe());
        }

        public Maybe<T> Else(Maybe<T> maybeAlternative)
        {
            if (HasValue)
            {
                return this;
            }

            return maybeAlternative;
        }

        public T Else(Func<T> alternativeFunc)
        {
            return Match(value => value, alternativeFunc);
        }

        public T Else(T alternative)
        {
            return Else(() => alternative);
        }

        public Maybe<T> Do(Action<T> action)
        {
            if (HasValue)
            {
                action(Value);
            }

            return this;
        }

        public Maybe<T> ElseDo(Action action)
        {
            if (!HasValue)
            {
                action();
            }

            return this;
        }

        public Maybe<T> Where(Predicate<T> predicate)
        {
            if (HasValue)
            {
                return predicate(Value)
                    ? this
                    : None;
            }

            return None;
        }

        public static bool operator ==(Maybe<T> left, Maybe<T> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Maybe<T> left, Maybe<T> right)
        {
            return !left.Equals(right);
        }

        public bool Equals(Maybe<T> other)
        {
            if (HasValue == other.HasValue)
            {
                if (!HasValue)
                {
                    return true;
                }

                return _value.Equals(other._value);
            }

            return false;
        }

        public override bool Equals(object obj)
        {
            return obj is Maybe<T> other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HasValue ? Value.GetHashCode() : 0;
        }

        public override string ToString()
        {
            return HasValue ? $"Maybe({_value})" : "None";
        }

        public Maybe<TResult> Cast<TResult>()
        {
            if (!HasValue)
            {
                return Maybe<TResult>.None;
            }

            return new Maybe<TResult>((TResult) (object) _value);
        }
    }

    public static class MaybeExtensions
    {
        public static Maybe<T> AsMaybe<T>(this T? value) where T : struct
        {
            return value.HasValue ? new Maybe<T>(value.Value) : Maybe<T>.None;
        }

        public static T? AsNullable<T>(this Maybe<T> maybeValue) where T : struct
        {
            return maybeValue.HasValue ? maybeValue.Value : new T?();
        }

        public static Maybe<T> AsMaybe<T>(this T value)
        {
            return ReferenceEquals(null, value) ? Maybe<T>.None : new Maybe<T>(value);
        }

        public static Maybe<T> MaybeWithIndex<T>(this T[] array, int index)
        {
            if (array.Length <= index || index < 0)
            {
                return Maybe<T>.None;
            }

            return array[index].AsMaybe();
        }

        public static Maybe<TEntry> MaybeFirst<TEntry>(this IEnumerable<TEntry> enumerable)
        {
            return enumerable.FirstOrDefault().AsMaybe();
        }

        public static Maybe<TEntry> MaybeFirst<TEntry>(this IEnumerable<TEntry> enumerable, Func<TEntry, bool> predicate)
        {
            return enumerable.Where(predicate).MaybeFirst();
        }

        public static Maybe<TEntry> MaybeLast<TEntry>(this IEnumerable<TEntry> enumerable)
        {
            return enumerable.LastOrDefault().AsMaybe();
        }

        public static Maybe<TEntry> MaybeLast<TEntry>(this IEnumerable<TEntry> enumerable, Func<TEntry, bool> predicate)
        {
            return enumerable.Where(predicate).MaybeLast();
        }
    }
}